class Animal:
    def __init__(self, name, age, legs):
        self.name = name
        self.age = age

animal = Animal('Ben', '30')

animal.name
'Ben'

animal.age
'30'

class Animal:
    def __init__(self, name, age, legs):
        self.name = name
        self.age = age
    def __len__(self):
        return 99

len(Animal)
99

class Animal:
    def __init__(self, name, age, legs):
        self.name = name
        self.age = age
        self.legs = legs
    def __str__(self):
        return 'Benjapol'
class Dog(Animal):
    def __init__(self, name, age):
        super().__init__(name, age, 4)


dog1 = Dog('First', '30')
dog2 = Dog('Tle', '10')
dog3 = Dog('Ben', '45')

dog1.species
'mammal'

dog1.name
'First'

dog1.description()
'First is 30 years old'

