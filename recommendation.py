import csv

from sklearn import model_selection

from typing import Tuple

from apyori import apriori

class RecommendationManager:
    def __init__(self, data_file_name: str, random_state: float =1, 
        test_size: float =0.1, 
        min_support: float =0.0045, 
        min_confidence: float =0.2,
        min_lift: float =1.5):
        #self.load_data(data_file_name)
    
        self.DATA = self.load_data(data_file_name)
        self.TRAINING_DATA, self.TESTING_DATA = self.split_training_and_testing_data(
            self.DATA, random_state, test_size)
        #self.RELATIONS = RecommendationManager
        self.RELATIONS = self.find_relations(self.TRAINING_DATA, min_support, min_confidence, min_lift)
        self.RECOMMENDATION_RULES = self.make_recommendation_rules(self.RELATIONS)

    @staticmethod
    #def load_data(self, data_file_name):
    def load_data(data_file_name: str) -> list:
        data = []
        with open(data_file_name) as data_file:
            csv_reader = csv.reader(data_file, skipinitialspace=True)
            for row in csv_reader:
                data.append(row)
        #self.data = data_file_name
        return data

    @staticmethod
    def split_training_and_testing_data(data: list, random_state: float, test_size: float) -> Tuple[list, list]:
        training_data, testing_data = model_selection.train_test_split(data, random_state=random_state, test_size=test_size)
        return(training_data, testing_data)
    
    @staticmethod
    def find_relations(data: list, min_support: float, min_confidence: float, min_lift: float) -> list:
        relations = apriori(data, min_support=min_support, 
                                    min_confidence=min_confidence, 
                                    min_lift=min_lift)
        #return sorted(list(relations))
        return list(relations)

    @staticmethod
    def make_recommendation_rules(relations: list) -> list:
        rules = []
        for relation in relations:
            stat = relation.ordered_statistics[0]
            rules.append({
                "items": relation.items,
                "items_base": stat.items_base,
                "items_add": stat.items_add,
                "lift": stat.lift
            })
        return rules


    def get_recommendations(self, items_in_cart: list) -> list:
        recommendations = []
        items = frozenset(items_in_cart)
        rules = self.RECOMMENDATION_RULES
        for rule in rules:
                if rule['items_base'] <= items:
                    recommendations.append({
                        "items" : list(rule['items_add'])[0],
                        "strength" : rule['lift']
                     })
        return sorted(recommendations, key=lambda recommendation: recommendation['strength'], reverse=True)


if __name__ == "__main__":
    recommendation_manager = RecommendationManager('store_data.csv')
    recommended_items = recommendation_manager.get_recommendations(['chocolate'])
    #print(recommandation_manager.DATA)
    print(recommended_items)
